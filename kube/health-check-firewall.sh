#!/bin/bash

NETWORK_NAME=default

gcloud compute firewall-rules create fw-allow-health-checks \
    --network=$NETWORK_NAME \
    --action=ALLOW \
    --direction=INGRESS \
    --source-ranges=35.191.0.0/16,130.211.0.0/22 \
    --target-tags=allow-health-checks \
    --rules=tcp

gcloud compute firewall-rules create fw-allow-network-lb-health-checks \
    --network=$NETWORK_NAME \
    --action=ALLOW \
    --direction=INGRESS \
    --source-ranges=35.191.0.0/16,209.85.152.0/22,209.85.204.0/22 \
    --target-tags=allow-network-lb-health-checks \
    --rules=tcp